import { Component } from '@angular/core';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.css']
})
export class FieldComponent {
  openBlock = true;
  blocks = ['block1', 'block2', 'block3', 'block4', 'block5', 'block6',
            'block7', 'block8', 'block9', 'block10', 'block11', 'block12',
            'block13', 'block14', 'block15', 'block16', 'block17', 'block18',
            'block19', 'block20', 'block21', 'block22', 'block23', 'block24',
            'block25', 'block26', 'block27', 'block28', 'block29', 'block30',
            'block31', 'block32', 'block33', 'block34', 'block35', 'block36',];

  getBlockClass(i: number) {
    const className = this.openBlock ? 'grey' : 'white';
    return ['block', className];
  }
}
