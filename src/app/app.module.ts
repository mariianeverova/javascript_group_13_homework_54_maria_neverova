import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FieldComponent } from './field/field.component';
import { BlockComponent } from './block/block.component';
import { TriesComponent } from './tries/tries.component';

@NgModule({
  declarations: [
    AppComponent,
    FieldComponent,
    BlockComponent,
    TriesComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
