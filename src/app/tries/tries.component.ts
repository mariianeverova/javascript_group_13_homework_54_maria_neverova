import { Component } from '@angular/core';

@Component({
  selector: 'app-tries',
  templateUrl: './tries.component.html',
  styleUrls: ['./tries.component.css']
})
export class TriesComponent {
  tries = 0;
}
